<?php

use ImmoweltHH\FreeKick\Application\Application;
use ImmoweltHH\FreeKick\Application\FreekickApplicationConfig;

require_once "vendor/autoload.php";

Application::configure(new FreekickApplicationConfig());
Application::getInstance()->run();

// Gregor Münster -> IoT freischaltung
