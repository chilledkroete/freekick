<?php

namespace ImmoweltHH\FreeKick;

use DateInterval;
use DateTime;

require_once "MysqlWrapper.php";

class StatusHandler
{

    const DB_DATE_FORMAT = "Y-m-d H:i:s";

    /** @var MysqlWrapper */
    private $mysqlWrapper;

    /**
     * StatusHandler constructor.
     *
     * @param MysqlWrapper $mysqlWrapper
     */
    public function __construct(MysqlWrapper $mysqlWrapper)
    {
        $this->mysqlWrapper = $mysqlWrapper;
        $this->mysqlWrapper->connect();
    }

    public function __destruct()
    {
        $this->mysqlWrapper->close();
    }

    /**
     * @param int $value
     */
    public function updateStatus($value)
    {
        $query = "INSERT INTO history (status) VALUES (%d)";

        $this->mysqlWrapper->query(sprintf($query, $value));
    }

    /**
     * @return bool
     */
    public function isOccupied()
    {
        return $this->hasActiveMotion() || $this->hasActiveReservation();
    }

    /**
     * @return int
     */
    private function hasActiveMotion()
    {
        $query = "SELECT * FROM history ORDER BY created DESC LIMIT 1";
        $result = $this->mysqlWrapper->query($query);
        $resultArray = $result->fetch_assoc();

        return (int)$resultArray['status'];
    }

    private function hasActiveReservation()
    {
        $query = "select * FROM reservations WHERE created > '%s' ORDER BY created DESC ";

        $dateTime = new DateTime();
        $dateTime->sub(new DateInterval("PT2M"));

        $result = $this->mysqlWrapper->query(
            sprintf(
                $query,
                $dateTime->format(static::DB_DATE_FORMAT))
        );

        return $result->num_rows > 0;
    }

    public function reserveTable()
    {
        $query = "INSERT INTO reservations (created) VALUES ('%s')";

        $dateTime = new DateTime();
        $this->mysqlWrapper->query(
            sprintf(
                $query,
                $dateTime->format(static::DB_DATE_FORMAT)
            )
        );
    }
}
