<?php

namespace ImmoweltHH\FreeKick\Application;

use Exception;
use ImmoweltHH\DependencyInjection\InjectionContainer;
use ImmoweltHH\FreeKick\Application\Controller\AbstractController;
use ImmoweltHH\FreeKick\Application\ControllerAction\AbstractControllerAction;
use ImmoweltHH\FreeKick\Application\ControllerAction\ErrorAction;
use ImmoweltHH\FreeKick\Application\Exception\ApplicationConfigurationException;
use ImmoweltHH\FreeKick\Application\Exception\InvalidControllerActionException;
use ImmoweltHH\FreeKick\Application\Exception\InvalidControllerException;
use ImmoweltHH\FreeKick\Application\Exception\InvalidUserException;
use ImmoweltHH\FreeKick\Application\Permissions\PermissionService;
use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Request\RequestParser;
use ImmoweltHH\FreeKick\Application\Response\HttpStatus;
use ImmoweltHH\FreeKick\Application\Response\Response;
use InvalidArgumentException;

/**
 * Class Application
 * @package de\System
 */
class Application
{

    /** @var ApplicationConfig */
    private static $configuration = null;

    /** @var Application */
    private static $instance = null;

    /** @var bool */
    private static $isInitialized = false;

    /** @var InjectionContainer */
    private $diContainer;

    /**
     * @param ApplicationConfig $config
     *
     * @throws ApplicationConfigurationException
     */
    public static function configure(ApplicationConfig $config)
    {
        if (static::$isInitialized === false) {
            static::$configuration = $config;
        } else {
            throw new ApplicationConfigurationException("Application already initialized");
        }
    }

    /**
     * @param string $service
     *
     * @return object
     */
    public static function getService($service)
    {
        return static::getInstance()->diContainer->get($service);
    }

    /**
     * @return Application
     * @throws ApplicationConfigurationException
     */
    public static function getInstance()
    {
        if (static::$configuration == null) {
            throw new ApplicationConfigurationException("Application not configured");
        }

        if (static::$instance === null) {
            $application = new Application();
            $application->initialize();

            static::$instance = $application;
        }

        return static::$instance;
    }

    /**
     * Initializes the application, setting up controllers and models
     */
    public function initialize()
    {
        $this->initializeDIContainer();
        static::$isInitialized = true;
    }

    private function initializeDIContainer()
    {
        $this->diContainer = new InjectionContainer(static::$configuration);
    }

    /**
     * @param string $controller
     * @param string[] $params
     *
     * @return string
     */
    public
    static function buildUrl(
        $controller, array $params = []
    ) {
        $paramString = "";
        foreach ($params as $key => $value) {
            $paramString .= sprintf(
                "&%s=%s",
                $key,
                $value
            );
        }

        return sprintf(
            "index.php?controller=%s%s",
            $controller,
            $paramString
        );
    }

    /**
     * Returns true if the application has already been used/initialized, false otherwise
     *
     * @return bool
     */
    public static function isInitialized()
    {
        return static::$isInitialized;
    }

    /**
     * Returns true if the configuration has been set, false otherwise
     *
     * @return bool
     */
    public static function isConfigured()
    {
        return static::$configuration instanceof ApplicationConfig;
    }

    /**
     * Handles the request
     */
    public function run()
    {
        $this->fixPostBody();
        $request = $this->parseRequest();

        try {
            $controllerName = $request->getController();
            $controller = $this->getController($controllerName);
        } catch (InvalidControllerException $invalidControllerException) {
            error_log($invalidControllerException->getMessage());
            http_response_code(404);

            return;
        }

        try {
            $this->executeController($controller, $request);
        } catch (InvalidControllerActionException $e) {
            $action = $controller->getErrorAction();
            $response = $controller->createResponse();
            $action->execute($request, $response);
        }
    }

    /**
     * POST bodies sent as JSON are not automatically parsed and put into
     * the $_POST array, thus we have to do it by hand
     */
    private function fixPostBody()
    {
        if ($_SERVER["REQUEST_METHOD"] === 'POST' && empty($_POST)) {
            $_POST = json_decode(file_get_contents('php://input'), true);
        }
    }

    /**
     * @return Request
     */
    private function parseRequest()
    {
        $requestParser = new RequestParser();

        return $requestParser->parse();
    }

    /**
     * @param string $controllerName
     *
     * @return AbstractController
     * @throws InvalidControllerException
     * @throws Exception
     */
    private function getController($controllerName)
    {
        if ($controllerName == null) {
            $controllerName = "Index";
        }

        if (!$this->diContainer->hasAlias($controllerName)) {
            $this->throwInvalidControllerException($controllerName);
        }

        $controller = $this->diContainer->getAliased($controllerName);

        if (!($controller instanceof AbstractController)) {
            $this->throwInvalidControllerException($controllerName);
        }

        return $controller;
    }

    /**
     * @param string $controllerName
     *
     * @throws InvalidControllerException
     */
    private function throwInvalidControllerException($controllerName)
    {
        throw new InvalidControllerException($controllerName);
    }

    /**
     * @param AbstractController $controller
     * @param Request $request
     */
    private function executeController(AbstractController $controller, Request $request)
    {
        /** @var PermissionService $permissionService */
        $permissionService = $this->diContainer->get(PermissionService::class);

        $response = $controller->createResponse();

        try {
            $actionName = $controller->getActionClass($request->getAction());

            /** @var AbstractControllerAction $action */
            $action = $this->diContainer->get($actionName);
            $action->setPermissionService($permissionService);
            $action->validatePermissions();
            $action->execute($request, $response);
        } catch (InvalidUserException $e) {
            $this->handleException($controller, $e, $request, $response, HttpStatus::HTTP_UNAUTHORIZED);
        } catch (InvalidControllerActionException $e) {
            $this->handleException($controller, $e, $request, $response, HttpStatus::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            $this->handleException($controller, $e, $request, $response);
        }

        $response->render();
    }

    /**
     * @param AbstractController $controller
     * @param Exception $e
     * @param Request $request
     * @param Response $response
     * @param int $statusCode
     */
    private function handleException(
        AbstractController $controller,
        Exception $e,
        Request $request,
        Response $response,
        $statusCode = null
    ) {
        $displayedStatusCode = HttpStatus::HTTP_INTERNAL_SERVER_ERROR;
        if ($statusCode === null && $e->getCode() !== null) {
            $displayedStatusCode = $e->getCode();
        } elseif ($statusCode !== null) {
            $displayedStatusCode = $statusCode;
        }

        /** @var ErrorAction $action */
        $action = $controller->getErrorAction();
        $action->setMessage($e->getMessage());
        $action->setStatusCode($displayedStatusCode);
        $action->setException($e);
        $action->execute($request, $response);

        error_log($e->getMessage());
    }

    /**
     * @return InjectionContainer
     * @deprecated
     */
    public function getDIContainer()
    {
        if (static::$isInitialized) {
            return $this->diContainer;
        }

        throw new InvalidArgumentException("Application context not initialized");
    }
}
