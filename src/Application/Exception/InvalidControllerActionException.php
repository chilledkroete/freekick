<?php

namespace ImmoweltHH\FreeKick\Application\Exception;

use Exception;

class InvalidControllerActionException extends Exception
{
    public function __construct($actionName, $controllerName)
    {
        parent::__construct(
            sprintf(
                "Invalid Action '%s' for controller '%s'",
                $actionName,
                $controllerName
            )
        );
    }
}
