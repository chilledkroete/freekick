<?php

namespace ImmoweltHH\FreeKick\Application\Exception;

use Exception;

/**
 * Class ApplicationException
 * @package ImmoweltHH\FreeKick\Exception
 */
class ApplicationConfigurationException extends Exception
{

}
