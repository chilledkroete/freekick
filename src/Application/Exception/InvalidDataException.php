<?php

namespace ImmoweltHH\FreeKick\Exception;

use Exception;

/**
 * Class InvalidDataException
 * @package ImmoweltHH\FreeKick\Exception
 */
class InvalidDataException extends Exception
{

}
