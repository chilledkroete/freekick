<?php

namespace ImmoweltHH\FreeKick\Application\Exception;

use Exception;

class InvalidUserException extends Exception
{
    public function __construct()
    {
        parent::__construct("Rechte nicht ausreichend");
    }
}
