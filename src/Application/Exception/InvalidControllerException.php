<?php

namespace ImmoweltHH\FreeKick\Application\Exception;

use Exception;

class InvalidControllerException extends Exception
{
    public function __construct($controllerName)
    {
        parent::__construct(sprintf("Controller \"%s\" nicht gefunden", $controllerName));
    }
}
