<?php

namespace ImmoweltHH\FreeKick\Application;

use ImmoweltHH\DependencyInjection\InjectionContainer;
use ImmoweltHH\FreeKick\Status\StatusController;
use ImmoweltHH\FreeKick\Update\UpdateController;

class FreekickApplicationConfig extends ApplicationConfig
{

    /**
     * FreekickApplicationConfig constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getProject()
    {
        return "unused";
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        return "unused";
    }

    /**
     * @return string
     */
    public function getConfigPath()
    {
        return "unused";
    }

    /**
     * Returns a map of alias => class name
     *
     * @return string[]
     */
    public function aliases()
    {
        return [
            StatusController::name() => StatusController::class,
            UpdateController::name() => UpdateController::class
        ];
    }

    /**
     * Returns a map of interface => class name
     *
     * @return string[]
     */
    public function interfaces()
    {
        return [];
    }

    /**
     * Returns a list of objects that should be used for instanciation
     * instead of a clean object
     *
     * @param InjectionContainer $container
     *
     * @return object[]
     */
    public function preconfiguredClasses(InjectionContainer $container)
    {
        return [];
    }

    /**
     * Return true to enable debug print
     *
     * @return boolean
     */
    public function debug()
    {
        return false;
    }
}
