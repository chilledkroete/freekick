<?php

namespace ImmoweltHH\FreeKick\Application\ControllerAction;

use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Response\Response;
use ImmoweltHH\FreeKick\Exception\InvalidUserException;

interface ControllerAction
{

    /**
     * @return string
     */
    public static function name();

    /**
     * @param Request $request
     * @param Response $response
     */
    public function execute(Request $request, Response $response);

    /**
     * @throws InvalidUserException
     */
    public function validatePermissions();
}
