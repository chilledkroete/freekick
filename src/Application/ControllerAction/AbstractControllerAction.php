<?php

namespace ImmoweltHH\FreeKick\Application\ControllerAction;

use ImmoweltHH\FreeKick\Application\Permissions\Permissions;
use ImmoweltHH\FreeKick\Application\Permissions\PermissionService;
use ImmoweltHH\FreeKick\Exception\InvalidUserException;

abstract class AbstractControllerAction implements ControllerAction
{

    /** @var PermissionService */
    private $permissionService;

    /**
     * @param PermissionService $permissionService
     */
    final public function setPermissionService(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * @throws InvalidUserException
     */
    public function validatePermissions()
    {
    }
}
