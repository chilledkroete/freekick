<?php

namespace ImmoweltHH\FreeKick\Application\ControllerAction;

use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Response\Response;
use stdClass;

class JsonErrorAction extends ErrorAction
{

    public function execute(Request $request, Response $response)
    {
        $errorObject = new stdClass();
        $errorObject->message = $this->getMessage();
        $errorObject->statusCode = $this->getStatusCode();

        $response->setRenderContent($errorObject);
    }

    public function validatePermissions() {
        // no permissions needed to execute error action
    }

    public static function name()
    {
        // TODO: Implement name() method.
    }
}
