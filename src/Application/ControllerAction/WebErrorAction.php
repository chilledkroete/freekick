<?php

namespace ImmoweltHH\FreeKick\Application\ControllerAction;

use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Response\Response;

class WebErrorAction extends ErrorAction
{

    public static function name()
    {
    }

    public function execute(Request $request, Response $response)
    {
        $renderContent = $this->getMessage();
        $response->setRenderContent($renderContent);
    }

    public function validatePermissions()
    {
    }
}
