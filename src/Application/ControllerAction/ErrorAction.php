<?php

namespace ImmoweltHH\FreeKick\Application\ControllerAction;

use ImmoweltHH\FreeKick\Application\Response\HttpStatus;
use Exception;

abstract class ErrorAction implements ControllerAction
{

    /** @var string */
    protected $message;

    /** @var int */
    protected $statusCode = HttpStatus::HTTP_INTERNAL_SERVER_ERROR;

    /** @var Exception */
    protected $errorException;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        http_response_code($this->statusCode);
    }

    /**
     * @param Exception $exception
     */
    public function setException(Exception $exception)
    {
        $this->errorException = $exception;
    }

    /**
     * @return Exception
     */
    public function getException()
    {
        return $this->errorException;
    }
}
