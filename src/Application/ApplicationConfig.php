<?php

namespace ImmoweltHH\FreeKick\Application;

use ImmoweltHH\DependencyInjection\InjectionConfig;
use ImmoweltHH\FreeKick\HttpClient\HttpClientFactory;

abstract class ApplicationConfig implements InjectionConfig
{

    /** @var HttpClientFactory */
    protected $httpClientFactory;

    /**
     * @param HttpClientFactory $httpClientFactory
     */
    public function setHttpClientFactory(HttpClientFactory $httpClientFactory)
    {
        $this->httpClientFactory = $httpClientFactory;
    }

    /**
     * @return string
     */
    abstract public function getProject();

    /**
     * @return string
     */
    abstract public function getEnvironment();

    /**
     * @return string
     */
    abstract public function getConfigPath();
}
