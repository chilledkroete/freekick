<?php

namespace ImmoweltHH\FreeKick\Application\Response;

class WebResponse extends Response
{
    /**
     * @inheritdoc
     */
    public function render()
    {
        if ($this->renderContent !== null) {
            echo $this->renderContent;
        }
    }
}
