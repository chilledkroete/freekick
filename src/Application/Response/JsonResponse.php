<?php

namespace ImmoweltHH\FreeKick\Application\Response;

use InvalidArgumentException;

/**
 * Class JsonResponse
 * @package ImmoweltHH\FreeKick\Request
 */
class JsonResponse extends Response
{

    public function render()
    {
        header('Content-Type: application/json; charset=utf-8');

        if ($this->renderContent !== null) {
            $json_encode = json_encode($this->renderContent);
            if (json_last_error() !== JSON_ERROR_NONE) {
                $message = json_last_error_msg();
                $code = json_last_error();
                throw new InvalidArgumentException(sprintf("An error happened during JSON encoding: [Err.Code: %d] %s", $code, $message));
            }

            if ($json_encode === false) {
                $json_encode = "[]";
            }

            echo $json_encode;
        }
    }
}
