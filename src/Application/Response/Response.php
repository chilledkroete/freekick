<?php

namespace ImmoweltHH\FreeKick\Application\Response;

abstract class Response
{
    /** @var mixed */
    protected $renderContent;

    public function __construct($renderContent = null)
    {
        $this->renderContent = $renderContent;
    }

    /**
     * @return void
     */
    abstract public function render();

    /**
     * @param null $renderContent
     */
    public function setRenderContent($renderContent)
    {
        $this->renderContent = $renderContent;
    }
}
