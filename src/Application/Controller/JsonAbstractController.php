<?php

namespace ImmoweltHH\FreeKick\Application\Controller;

use ImmoweltHH\FreeKick\Application\ControllerAction\JsonErrorAction;
use ImmoweltHH\FreeKick\Application\Response\JsonResponse;

abstract class JsonAbstractController extends AbstractController
{

    /**
     * @return JsonResponse
     */
    public function createResponse()
    {
        return new JsonResponse();
    }

    /**
     * @return JsonErrorAction
     */
    final public function getErrorAction()
    {
        return new JsonErrorAction();
    }
}
