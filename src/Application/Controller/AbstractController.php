<?php

namespace ImmoweltHH\FreeKick\Application\Controller;

use ImmoweltHH\FreeKick\Exception\InvalidControllerActionException;

/**
 * Interface Controller
 * @package de\System
 */
abstract class AbstractController implements Controller
{

    /**
     * @param string $actionName
     *
     * @return string
     * @throws InvalidControllerActionException
     */
    final public function getActionClass($actionName)
    {
        $controllerActions = $this->actions();
        if (array_key_exists($actionName, $controllerActions)) {
            return $controllerActions[$actionName];
        }

        throw new InvalidControllerActionException($actionName, static::name());
    }

    /**
     * @return string[]
     */
    abstract protected function actions();
}
