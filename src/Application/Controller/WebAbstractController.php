<?php

namespace ImmoweltHH\FreeKick\Application\Controller;

use ImmoweltHH\FreeKick\Application\ControllerAction\WebErrorAction;
use ImmoweltHH\FreeKick\Application\Response\WebResponse;

abstract class WebAbstractController extends AbstractController
{

    /**
     * @return WebResponse
     */
    public final function createResponse()
    {
        return new WebResponse();
    }

    /**
     * @return WebErrorAction
     */
    final public function getErrorAction()
    {
        return new WebErrorAction();
    }
}
