<?php

namespace ImmoweltHH\FreeKick\Application\Controller;

use ImmoweltHH\FreeKick\Application\ControllerAction\ErrorAction;
use ImmoweltHH\FreeKick\Application\Response\Response;

interface Controller
{
    /**
     * @return Response
     */
    public function createResponse();

    /**
     * @return string
     */
    public static function name();

    /**
     * @return ErrorAction
     */
    public function getErrorAction();
}
