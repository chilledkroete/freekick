<?php

namespace ImmoweltHH\FreeKick\Application\Permissions;

/**
 * This class is a leftover dependency from UMDEV-876 and served as permission validator
 * @package ImmoweltHH\FreeKick\Permissions
 */
class PermissionService
{

    public function dummyCheckPermission()
    {
        return true;
    }
}
