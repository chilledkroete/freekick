<?php

namespace ImmoweltHH\FreeKick\Application\Permissions;

class Permissions
{
    const NONE = null;

    const QUALIFICATION_OVERVIEW = 'qualification_overview';
    const QUALIFICATION_STATISTICS = 'qualification_statistics';
    const INVOICE_OVERVIEW = 'invoice_overview';
    const INVOICE_CSV = 'invoice_csv';
    const INVOICE_APPOINTMENTS = 'invoice_appointments';
    const INVOICE_STATISTICS = 'invoice_statistics';
    const INVOICE_REVERSAL = 'invoice_reversal';
    const EMPLOYEES_LEAD_ASSIGNMENT_OVERVIEW = 'employees_leadAssignmentOverview';
    const EMPLOYEES_DELETE = 'employees_delete';
    const EMPLOYEES_EDIT = 'employees_edit';
    const EMPLOYEES_VIEW = 'employees_view';
    const EMPLOYEES_STATISTICS = 'employees_statistics';
    const AUCTIONS_CANCELLED = 'auctions_cancelled';
    const AUCTIONS_WELCOME = 'auctions_welcome';
    const AUCTIONS_AUCTION_END = 'auctions_auctionEnd';
    const AUCTIONS_CONTACT_INQUIRY = 'auctions_contactInquiry';
    const AUCTIONS_LEGACY_OFFER_PHASE_ALL = 'auctions_legacyOfferPhase_all';
    const AUCTIONS_LEGACY_OFFER_PHASE_NO_APPOINTMENT = 'auctions_legacyOfferPhase_noAppointment';
    const AUCTIONS_OFFER_PHASE_DONE = 'auctions_offerPhaseDone';
    const AUCTIONS_NEW_OFFER_PHASE_ALL = 'auctions_newOfferPhase_all';
    const AUCTIONS_BASELINE_BIDDING = 'auctions_baselineBidding';
    const RESUBMISSION_CANCELLED = 'resubmission_cancelled';
    const RESUBMISSION_WELCOME = 'resubmission_welcome';
    const RESUBMISSION_OFFER_PHASE = 'resubmission_offerPhase';
    const RESUBMISSION_AUCTION_END = 'resubmission_auctionEnd';
    const MESSAGES_CUSTOMER = 'messages_customer';
    const MESSAGES_COMPANY = 'messages_company';
    const MESSAGES_CONSULTANT = 'messages_consultant';
    const CONTACT_URGENT = 'contact_urgent';
    const CONTACT_UPCOMING = 'contact_upcoming';
    const LISTS_COMPANIES_OVERVIEW = 'lists_companies_overview';
    const LISTS_COMPANIES_CSV = 'lists_companies_csv';
    const LISTS_COOPERATIONS = 'lists_cooperations';
    const LISTS_BIDS = 'lists_bids';
    const LISTS_RATINGS = 'lists_ratings';
    const JOBMANAGER_EMAIL_TEMPLATES = 'jobmanager_emailTemplates';
    const JOBMANAGER_SETTINGS = 'jobmanager_settings';
    const JOBMANAGER_COMPANY_COMPARISON = 'jobmanager_companyComparison';
    const JOBMANAGER_IMMONET = 'jobmanager_immonet';
    const JOBMANAGER_ERRORS = 'jobmanager_errors';
    const STATISTICS_AUCTIONS = 'statistics_auctions';
    const STATISTICS_INQUIRIES = 'statistics_inquiries';
    const STATISTICS_COOPERATIONS = 'statistics_cooperations';
    const STATISTICS_CANCELLATION_AVOIDED = 'statistics_cancellationAvoided';
    const OFFERS_UPLOAD = 'offers_upload';
    const OFFERS_INFO = 'offers_info';
    const OFFERS_LINK = 'offers_link';
    const OFFERS_STATE = 'offers_state';
    const BIDDING_CALLBACK_APPOINTMENT = 'bidding_callbackAppointment';
    const BIDDING_DELETE_BIDS = 'bidding_deleteBids';
    const BIDDING_INDUCE_CONTACT = 'bidding_induceContact';
    const BIDDING_LIST = 'bidding_list';
    const BIDDING_SET_EKOMI = 'bidding_setEkomi';
    const BIDDING_SUGGEST_COMPANIES = 'bidding_suggestCompanies';
    const BIDDING_RECOMMENDATIONS = 'bidding_recommendations';
    const CUSTOMER_CONTACTS = 'customerContact';
    const AUCTIONS_INFO_CUSTOMER_NOT_AVAILABLE = 'auctions_info_customerNotAvailable';
    const AUCTIONS_INFO_NEW_APPOINTMENT = 'auctions_info_newAppointment';
}
