<?php

namespace ImmoweltHH\FreeKick\Application\Request;

class Request
{
    /** @var string */
    private $controller;

    /** @var string */
    private $action;

    /** @var mixed[] */
    private $post;

    /** @var mixed[] */
    private $get;

    /**
     * Request constructor.
     * @param string $controller
     * @param string $action
     * @param mixed[] $get
     * @param mixed[] $post
     */
    public function __construct($controller, $action, $get, $post)
    {
        $this->controller = $controller;
        $this->action = $action;
        $this->get = $get;
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getPostValue($key) {
        if (isset($this->post[$key])) {
            return $this->post[$key];
        }

        return null;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getGetValue($key) {
        if (isset($this->get[$key])) {
            return $this->get[$key];
        }

        return null;
    }
}
