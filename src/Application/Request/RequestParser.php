<?php

namespace ImmoweltHH\FreeKick\Application\Request;

class RequestParser
{

    const FIELD_CONTROLLER = 'controller';
    const FIELD_ACTION = 'action';

    public function parse()
    {
        $controller = $this->extractControllerName();
        $action = $this->extractAction();

        return new Request(
            $controller,
            $action,
            $_GET,
            $_POST
        );
    }

    /**
     * @return string
     */
    private function extractControllerName()
    {
        $controllerName = "Index";

        if (isset($_GET[static::FIELD_CONTROLLER]) && $this->isValid($_GET[static::FIELD_CONTROLLER])) {
            $controllerName = $_GET[static::FIELD_CONTROLLER];
            unset ($_GET[static::FIELD_CONTROLLER]);
        }

        return $controllerName;
    }

    /**
     * @return string
     */
    private function extractAction()
    {
        $actionName = "indexAction";

        if (isset($_GET[static::FIELD_ACTION]) && $this->isValid($_GET[static::FIELD_ACTION])) {
            $actionName = $_GET[static::FIELD_ACTION];
            unset ($_GET[static::FIELD_ACTION]);
        } else if (isset($_POST[static::FIELD_ACTION]) && $this->isValid($_POST[static::FIELD_ACTION])) {
            $actionName = $_POST[static::FIELD_ACTION];
        }

        return $actionName;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    private function isValid($value)
    {
        return $value != null && $value !== "" && is_numeric($value) === false;
    }
}
