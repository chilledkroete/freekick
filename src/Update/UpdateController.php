<?php

namespace ImmoweltHH\FreeKick\Update;

use ImmoweltHH\FreeKick\Application\Controller\WebAbstractController;

class UpdateController extends WebAbstractController
{

    /**
     * @return string[]
     */
    protected function actions()
    {
        return [
            IndexAction::name() => IndexAction::class
        ];
    }

    /**
     * @return string
     */
    public static function name()
    {
        return "update";
    }
}
