<?php

namespace ImmoweltHH\FreeKick\Update;

use ImmoweltHH\FreeKick\Application\ControllerAction\AbstractControllerAction;
use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Response\Response;
use ImmoweltHH\FreeKick\StatusHandler;

class IndexAction extends AbstractControllerAction
{

    /** @var StatusHandler */
    private $statusHandler;

    /**
     * IndexAction constructor.
     *
     * @param StatusHandler $statusHandler
     */
    public function __construct(StatusHandler $statusHandler)
    {
        $this->statusHandler = $statusHandler;
    }

    /**
     * @return string
     */
    public static function name()
    {
        return "indexAction";
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function execute(Request $request, Response $response)
    {
        $anyoneThere = $request->getGetValue("anyoneThere");

        if (!$this->validateInput($anyoneThere)) {
            http_response_code(400);
            $response->setRenderContent("Invalid Input");

            return;
        }

        $this->statusHandler->updateStatus($anyoneThere);
        $response->setRenderContent("OK");
    }

    /**
     * @param int $input
     *
     * @return bool
     */
    private function validateInput($input): bool
    {
        return in_array($input, [1, 0]);
    }
}
