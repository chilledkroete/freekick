<?php

namespace ImmoweltHH\FreeKick;

use mysqli;
use mysqli_result;

class MysqlWrapper
{

    /** @var mysqli */
    private $connection;

    public function connect()
    {
        $this->connection = new mysqli(
            "freekick.czyqngylmx0w.eu-central-1.rds.amazonaws.com",
            "freekick",
            "immoweltHH",
            "freekick"
        );
    }

    /**
     * @param string $sql
     *
     * @return bool|mysqli_result
     */
    public function query($sql)
    {
        return $this->connection->query($sql);
    }

    public function close()
    {
        $this->connection->close();
    }
}
