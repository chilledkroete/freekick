<?php

namespace ImmoweltHH\FreeKick\Status;

use ImmoweltHH\FreeKick\Application\ControllerAction\AbstractControllerAction;
use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Response\Response;
use ImmoweltHH\FreeKick\StatusHandler;

class ReserveAction extends AbstractControllerAction
{

    /** @var StatusHandler */
    private $statusHandler;

    /**
     * ReserveAction constructor.
     *
     * @param StatusHandler $statusHandler
     */
    public function __construct(StatusHandler $statusHandler)
    {
        $this->statusHandler = $statusHandler;
    }

    /**
     * @return string
     */
    public static function name()
    {
        return "reserve";
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function execute(Request $request, Response $response)
    {
        $this->statusHandler->reserveTable();
        header("Location: /");
    }
}
