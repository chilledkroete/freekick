<?php

namespace ImmoweltHH\FreeKick\Status;

use ImmoweltHH\FreeKick\Application\Controller\WebAbstractController;

class StatusController extends WebAbstractController
{

    /**
     * @return string
     */
    public static function name()
    {
        return "status";
    }

    /**
     * @return string[]
     */
    protected function actions()
    {
        return [
            IndexAction::name() => IndexAction::class,
            ReserveAction::name() => ReserveAction::class
        ];
    }
}
