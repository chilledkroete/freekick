<?php

namespace ImmoweltHH\FreeKick\Status;

use ImmoweltHH\FreeKick\Application\ControllerAction\AbstractControllerAction;
use ImmoweltHH\FreeKick\Application\Request\Request;
use ImmoweltHH\FreeKick\Application\Response\Response;
use ImmoweltHH\FreeKick\StatusHandler;
use ImmoweltHH\FreeKick\Template\Template;
use ImmoweltHH\FreeKick\Template\TemplateLoader;

class IndexAction extends AbstractControllerAction
{

    /** @var StatusHandler */
    private $statusHandler;

    /** @var TemplateLoader */
    private $templateLoader;

    /**
     * IndexAction constructor.
     *
     * @param StatusHandler $statusHandler
     * @param TemplateLoader $templateLoader
     */
    public function __construct(
        StatusHandler $statusHandler,
        TemplateLoader $templateLoader
    ) {
        $this->statusHandler = $statusHandler;
        $this->templateLoader = $templateLoader;
    }

    /**
     * @return string
     */
    public static function name()
    {
        return "indexAction";
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function execute(Request $request, Response $response)
    {
        $template = new Template(realpath(__DIR__ . "/status.phtml"));
        $template->put("status", $this->statusHandler->isOccupied());

        $response->setRenderContent($this->templateLoader->load($template)->getContent());
    }
}
