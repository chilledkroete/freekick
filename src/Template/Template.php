<?php

namespace ImmoweltHH\FreeKick\Template;

use InvalidArgumentException;

class Template
{

    const RESERVED_KEY_TEMPLATE = "template";

    /** @var string */
    private $path;
    /** @var mixed[] */
    private $params = [];
    /** @var string */
    private $content;

    /**
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function put($key, $value)
    {
        $this->assertKeyTemplate($key);

        $this->params[$key] = $value;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }


    /**
     * @param string $key
     */
    private function assertKeyTemplate($key)
    {
        if ($key == static::RESERVED_KEY_TEMPLATE) {
            throw new InvalidArgumentException("Key 'template' must not be used.");
        }
    }

    /**
     * @param string $buffer
     */
    public function handle($buffer)
    {
        $this->content = $buffer;
    }
}
