<?php

namespace ImmoweltHH\FreeKick\Template;

class TemplateLoader
{

    /**
     * @param Template $template
     *
     * @return Template
     * @throws FileDoesNotExistException
     */
    public function load(Template $template)
    {
        ob_start([$template, "handle"]);
        extract($template->getParams());

        $this->assertFileExists($template->getPath());
        require $template->getPath();

        ob_end_clean();

        return $template;
    }

    /**
     * @param string $file
     *
     * @throws FileDoesNotExistException
     */
    private function assertFileExists($file)
    {
        if (!file_exists($file)) {
            throw new FileDoesNotExistException(
                sprintf("file '%s' does not exist",
                    $file
                )
            );
        }
    }
}
