<?php

use ImmoweltHH\FreeKick\MysqlWrapper;
use ImmoweltHH\FreeKick\StatusHandler;

require_once "src/StatusHandler.php";

$handler = new StatusHandler(new MysqlWrapper());
$handler->reserveTable();

header("Location: status.php");
