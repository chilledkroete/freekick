<?php

use ImmoweltHH\FreeKick\MysqlWrapper;
use ImmoweltHH\FreeKick\StatusHandler;

require_once "src/StatusHandler.php";

$anyoneThere = $_GET['anyoneThere'];

$statusHandler = new StatusHandler(new MysqlWrapper());
$statusHandler->updateStatus($anyoneThere);
