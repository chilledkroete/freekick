<?php

use ImmoweltHH\FreeKick\MysqlWrapper;
use ImmoweltHH\FreeKick\StatusHandler;
use ImmoweltHH\FreeKick\Template\Template;
use ImmoweltHH\FreeKick\Template\TemplateLoader;

require_once "src/StatusHandler.php";

$handler = new StatusHandler(new MysqlWrapper());
$status = $handler->isOccupied();

$template = new Template("status_template.phtml");
$template->put("status", $handler->isOccupied());

$templateLoader = new TemplateLoader();
echo $templateLoader->load($template)->getContent();
